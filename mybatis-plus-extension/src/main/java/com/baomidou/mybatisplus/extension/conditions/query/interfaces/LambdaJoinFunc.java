package com.baomidou.mybatisplus.extension.conditions.query.interfaces;

import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaJoinQueryWrapper;

/**
 * join封装
 *
 * @author mahuibo
 * @since 2022/11/23
 */
public interface LambdaJoinFunc {

    default <X, J> LambdaJoinQueryWrapper<X> innerJoin(Class<X> joinClass, SFunction<X, ?> leftField, SFunction<J, ?> rightField) {
        return lambdaJoin(joinClass, leftField, rightField, null, Constants.INNER_JOIN);
    }

    default <X, J> LambdaJoinQueryWrapper<X> innerJoin(Class<X> joinClass, SFunction<X, ?> leftField, SFunction<J, ?> rightField, String alias) {
        return lambdaJoin(joinClass, leftField, rightField, alias, Constants.INNER_JOIN);
    }

    default <X, J> LambdaJoinQueryWrapper<X> leftJoin(Class<X> joinClass, SFunction<X, ?> leftField, SFunction<J, ?> rightField, String alias) {
        return lambdaJoin(joinClass, leftField, rightField, alias, Constants.LEFT_JOIN);
    }

    default <X, J> LambdaJoinQueryWrapper<X> leftJoin(Class<X> joinClass, SFunction<X, ?> leftField, SFunction<J, ?> rightField) {
        return lambdaJoin(joinClass, leftField, rightField, null, Constants.LEFT_JOIN);
    }

    default <X, J> LambdaJoinQueryWrapper<X> rightJoin(Class<X> joinClass, SFunction<X, ?> leftField, SFunction<J, ?> rightField, String alias) {
        return lambdaJoin(joinClass, leftField, rightField, alias, Constants.RIGHT_JOIN);
    }

    default <X, J> LambdaJoinQueryWrapper<X> rightJoin(Class<X> joinClass, SFunction<X, ?> leftField, SFunction<J, ?> rightField) {
        return lambdaJoin(joinClass, leftField, rightField, null, Constants.RIGHT_JOIN);
    }

    /**
     * join
     * 如果要添加joinClass的where条件,orderby ，groupby，请使用此接口返回值
     *
     * @param joinClass  join的表的类
     * @param leftField  左表链接字段
     * @param rightField 右表链接字段
     * @param alias      表别名
     * @param joinType   join类型 inner join left join right join
     * @return this
     */
    <X, J> LambdaJoinQueryWrapper<X> lambdaJoin(Class<X> joinClass, SFunction<X, ?> leftField, SFunction<J, ?> rightField, String alias, String joinType);


    default <X> LambdaJoinQueryWrapper<X> innerJoin(Class<X> joinClass) {
        return join(joinClass, null, Constants.INNER_JOIN);
    }

    default <X> LambdaJoinQueryWrapper<X> innerJoin(Class<X> joinClass, String alias) {
        return join(joinClass, null, Constants.INNER_JOIN);
    }

    default <X> LambdaJoinQueryWrapper<X> leftJoin(Class<X> joinClass, String alias) {
        return join(joinClass, null, Constants.LEFT_JOIN);
    }

    default <X> LambdaJoinQueryWrapper<X> leftJoin(Class<X> joinClass) {
        return join(joinClass, null, Constants.LEFT_JOIN);
    }


    default <X> LambdaJoinQueryWrapper<X> rightJoin(Class<X> joinClass, String alias) {
        return join(joinClass, null, Constants.RIGHT_JOIN);
    }

    default <X> LambdaJoinQueryWrapper<X> rightJoin(Class<X> joinClass) {
        return join(joinClass, null, Constants.RIGHT_JOIN);
    }

    /**
     * join
     * 如果要添加joinClass的where条件,orderby ，groupby，请使用此接口返回值
     *
     * @param joinClass join的表的类
     * @param joinType  join类型 inner join left join right join
     * @return this
     */
    <X> LambdaJoinQueryWrapper<X> join(Class<X> joinClass, String alias, String joinType);

}
