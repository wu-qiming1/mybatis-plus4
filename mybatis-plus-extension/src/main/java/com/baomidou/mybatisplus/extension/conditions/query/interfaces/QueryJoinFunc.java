package com.baomidou.mybatisplus.extension.conditions.query.interfaces;

import com.baomidou.mybatisplus.core.toolkit.Constants;

/**
 * join封装
 *
 * @author mahuibo
 * @since 2022/11/23
 */
public interface QueryJoinFunc<Children> {

    default Children innerJoin(Class<?> joinClass) {
        return join(joinClass, null, Constants.INNER_JOIN);
    }

    default Children innerJoin(Class<?> joinClass, String alias) {
        return join(joinClass, null, Constants.INNER_JOIN);
    }

    default Children leftJoin(Class<?> joinClass, String alias) {
        return join(joinClass, null, Constants.LEFT_JOIN);
    }

    default Children leftJoin(Class<?> joinClass) {
        return join(joinClass, null, Constants.LEFT_JOIN);
    }


    default Children rightJoin(Class<?> joinClass, String alias) {
        return join(joinClass, null, Constants.RIGHT_JOIN);
    }

    default Children rightJoin(Class<?> joinClass) {
        return join(joinClass, null, Constants.RIGHT_JOIN);
    }

    /**
     * join
     * 如果要添加joinClass的where条件,orderby ，groupby，请使用此接口返回值
     *
     * @param joinClass join的表的类
     * @param joinType  join类型 inner join left join right join
     * @param alias     别名
     * @return this
     */
    Children join(Class<?> joinClass, String alias, String joinType);

}
