/*
 * Copyright (c) 2011-2022, baomidou (jobob@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.baomidou.mybatisplus.annotation;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * MybatisPlus 表和表之间的关联关系
 * @author wanglei
 * @since 2022-03-16
 */
@Getter
@AllArgsConstructor
public enum Relation {

    NONE("none", "无关联关系"),
    ONE_TO_ONE("oneToOne", "一对一"),
    ONE_TO_MANY("oneToMany", "一对多"),
    MANY_TO_ONE("manyToOne", "多对一"),
    MANY_TO_MANY("manyToMany", "多对多");

    /**
     * 编码
     */
    private final String code;
    /**
     * 描述
     */
    private final String desc;

}
